<?php
/**
 * @file
 * Drupal Transformations -
 * Transformations for processing Drupal-native data like users, users, etc.
 *
 * Copyright 2009 by Jakob Petsovits ("jpetso", http://drupal.org/user/56020)
 */


/**
 * Helper class for providing implementations for standard input/output info.
 */
class TfUserConnectionInfoHelper {
  public static function userObjectInfo($propertyKey) {
    switch ($propertyKey) {
      case 'label':
        return t('User object');

      case 'expectedType':
        return 'drupal:user:object';
    }
  }

  public static function userFieldInfo($userField, $propertyKey) {
    switch ($propertyKey) {
      case 'label':
        return $userField['label'];

      default:
        $function = fieldtool_get_function(
          $userField, 'transformations slot property callback'
        );
        return $function($userField, $propertyKey);
    }
  }
}


/**
 * Implementation of [module]_operation_[class]().
 */
function transformations_drupal_operation_TfDrupalUserLoad() {
  return array(
    'category' => t('Drupal users'),
    'label' => t('Load Drupal user'),
    'description' => t('Retrieves a Drupal user object from the database.'),
  );
}

class TfDrupalUserLoad extends TfOperation {
  /**
   * Implementation of TfOperation::inputs().
   */
  protected function inputs() {
    return array('uid');
  }

  /**
   * Implementation of TfOperation::inputInfo().
   */
  protected function inputInfo($inputKey, $propertyKey) {
    if ($inputKey == 'uid') {
      switch ($propertyKey) {
        case 'label':
          return t('User id');

        case 'expectedType':
          return 'php:type:integer';
      }
    }
  }

  /**
   * Implementation of TfOperation::outputs().
   */
  protected function outputs() {
    return array('user');
  }

  /**
   * Implementation of TfOperation::outputInfo().
   */
  protected function outputInfo($outputKey, $propertyKey) {
    if ($outputKey == 'user') {
      return TfUserConnectionInfoHelper::userObjectInfo($propertyKey);
    }
  }

  /**
   * Implementation of TfOperation::execute().
   */
  protected function execute(TfOutput $output) {
    $uid = $this->input('uid')->data();
    $user = user_load($uid);

    if (!$user) {
      $output->setErrorMessage(t('Could not load user with uid !uid.', array(
        '!uid' => $uid,
      )));
      return;
    }
    $output->set('user', $user);
  }
}


/**
 * Implementation of [module]_operation_[class]().
 */
function transformations_drupal_operation_TfDrupalUserModify() {
  return array(
    'category' => t('Drupal users'),
    'label' => t('Modify & save existing Drupal user'),
    'description' => t('Writes a Drupal user object to the database with modifications.'),
  );
}

class TfDrupalUserModify extends TfOperation {
  /**
   * Implementation of TfOperation::inputs().
   */
  protected function inputs() {
    $inputs = array_keys(fieldtool_fields('user', array(
      'required keys' => array('transformations slot property callback', 'setter callback'),
    )));
    array_unshift($inputs, 'user');
    return $inputs;
  }

  /**
   * Implementation of TfOperation::inputInfo().
   */
  protected function inputInfo($inputKey, $propertyKey) {
    if ($inputKey == 'user') {
      switch ($propertyKey) {
        case 'label':
          return t('Existing user object');

        default:
          return TfUserConnectionInfoHelper::nodeObjectInfo($propertyKey);
      }
    }
    else { // handling field keys
      if ($propertyKey == 'required') {
        return FALSE;
      }
      $userFields = fieldtool_fields('user', array(
        'required keys' => array('transformations slot property callback', 'setter callback'),
      ));
      return TfUserConnectionInfoHelper::userFieldInfo($userFields[$inputKey], $propertyKey);
    }
  }

  /**
   * Implementation of TfOperation::outputs().
   */
  protected function outputs() {
    return array('savedUser');
  }

  /**
   * Implementation of TfOperation::outputInfo().
   */
  protected function outputInfo($outputKey, $propertyKey) {
    if ($outputKey == 'savedUser') {
      switch ($propertyKey) {
        case 'label':
          return t('Saved user object');

        default:
          return TfUserConnectionInfoHelper::userObjectInfo($propertyKey);
      }
    }
  }

  /**
   * Implementation of TfOperation::execute().
   */
  protected function execute(TfOutput $output) {
    $account = $this->input('user')->data();
    $savedAccount = clone $account;
    $modifiedFields = array();
    $userFieldInfo = fieldtool_fields('user', array(
      'required keys' => array('transformations slot property callback', 'setter callback'),
    ));
    if (isset($userFieldInfo['user'])) {
      // Not aware of any module that provides a field named 'user',
      // but let's make sure to avoid those in case there is such a thing.
      unset($userFieldInfo['user']);
    }

    foreach ($userFieldInfo as $fieldName => &$info) {
      if (!$this->isInputSet($fieldName)) {
        // The user decides which fields to set and which ones to leave as is.
        continue;
      }
      fieldtool_set($info, $savedAccount, $this->input($fieldName)->data());
      $modifiedFields[$fieldName] = $savedAccount->$fieldName;
    }

    $savedAccount = user_save($savedAccount, $modifiedFields);

    if (!$savedAccount) {
      $output->setErrorMessage(t('Could not save user object.'));
      return;
    }
    $output->set('savedUser', $savedAccount);
  }
}


/**
 * Implementation of [module]_operation_[class]().
 */
function transformations_drupal_operation_TfDrupalUserCreate() {
  return array(
    'category' => t('Drupal users'),
    'label' => t('Create & save new Drupal user'),
    'description' => t('Writes a new Drupal user object to the database.'),
  );
}

class TfDrupalUserCreate extends TfOperation {
  /**
   * Implementation of TfOperation::inputs().
   */
  protected function inputs() {
    $inputs = array_keys(fieldtool_fields('user', array(
      'required keys' => array('transformations slot property callback', 'setter callback'),
    )));
    return $inputs;
  }

  /**
   * Implementation of TfOperation::inputInfo().
   */
  protected function inputInfo($inputKey, $propertyKey) {
    // handling field keys
    $userFields = fieldtool_fields('user', array(
      'required keys' => array('transformations slot property callback', 'setter callback'),
    ));
    return TfUserConnectionInfoHelper::userFieldInfo($userFields[$inputKey], $propertyKey);
  }

  /**
   * Implementation of TfOperation::outputs().
   */
  protected function outputs() {
    return array('savedUser');
  }

  /**
   * Implementation of TfOperation::outputInfo().
   */
  protected function outputInfo($outputKey, $propertyKey) {
    if ($outputKey == 'savedUser') {
      switch ($propertyKey) {
        case 'label':
          return t('Saved user object');

        default:
          return TfUserConnectionInfoHelper::userObjectInfo($propertyKey);
      }
    }
  }

  /**
   * Implementation of TfOperation::execute().
   */
  protected function execute(TfOutput $output) {
    $savedAccount = new stdClass();
    $savedAccount->uid = 0; // Prevents a notice in user_save().

    $modifiedFields = array();
    $userFieldInfo = fieldtool_fields('user', array(
      'required keys' => array('transformations slot property callback', 'setter callback'),
    ));
    if (isset($userFieldInfo['user'])) {
      // Not aware of any module that provides a field named 'user',
      // but let's make sure to avoid those in case there is such a thing.
      unset($userFieldInfo['user']);
    }

    foreach ($userFieldInfo as $fieldName => &$info) {
      if (!$this->isInputSet($fieldName)) {
        // The user decides which fields to set and which ones to leave as is.
        continue;
      }
      fieldtool_set($info, $savedAccount, $this->input($fieldName)->data());
      $modifiedFields[$fieldName] = $savedAccount->$fieldName;
    }

    $savedAccount = user_save($savedAccount, $modifiedFields);

    if (!$savedAccount) {
      $output->setErrorMessage(t('Could not save user object.'));
      return;
    }
    $output->set('savedUser', $savedAccount);
  }
}


/**
 * Implementation of [module]_operation_[class]().
 */
function transformations_drupal_operation_TfDrupalUserFieldsExtract() {
  return array(
    'category' => t('Drupal users'),
    'label' => t('Extract fields from Drupal user object'),
    'description' => t('Retrieve data from an existing Drupal user object.'),
  );
}

class TfDrupalUserFieldsExtract extends TfOperation {
  /**
   * Implementation of TfOperation::inputs().
   */
  protected function inputs() {
    return array('user');
  }

  /**
   * Implementation of TfOperation::inputInfo().
   */
  protected function inputInfo($inputKey, $propertyKey) {
    if ($inputKey == 'user') {
      return TfUserConnectionInfoHelper::userObjectInfo($propertyKey);
    }
  }

  /**
   * Implementation of TfOperation::outputs().
   */
  protected function outputs() {
    return array_keys(fieldtool_fields('user', array(
      'required keys' => array('transformations slot property callback', 'getter callback'),
    )));
  }

  /**
   * Implementation of TfOperation::outputInfo().
   */
  protected function outputInfo($outputKey, $propertyKey) {
    $userFields = fieldtool_fields('user', array(
      'required keys' => array('transformations slot property callback', 'getter callback'),
    ));
    return TfUserConnectionInfoHelper::userFieldInfo($userFields[$outputKey], $propertyKey);
  }

  /**
   * Implementation of TfOperation::execute().
   */
  protected function execute(TfOutput $output) {
    $user = $this->input('user')->data();
    $userFieldInfo = fieldtool_fields('user', array(
      'required keys' => array('transformations slot property callback', 'getter callback'),
    ));
    foreach ($userFieldInfo as $fieldName => &$info) {
      $fieldData = fieldtool_get($info, $user);
      if (isset($fieldData)) {
        $output->set($fieldName, $fieldData);
      }
    }
  }
}
