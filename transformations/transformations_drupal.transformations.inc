<?php
/**
 * @file
 * Drupal Transformations -
 * Transformations for processing Drupal-native data like nodes, users, etc.
 *
 * Copyright 2009 by Jakob Petsovits ("jpetso", http://drupal.org/user/56020)
 */

/**
 * Implementation of hook_transformations_operation_info():
 * Return a registry of all the operations provided by this module.
 */
function transformations_drupal_transformations_operation_info() {
  $operations = array();

  $operations['TfDrupalNodeLoad'] = array(
    'file' => 'operations.node.inc',
  );
  $operations['TfDrupalNodeSave'] = array(
    'file' => 'operations.node.inc',
  );
  $operations['TfDrupalNodeNew'] = array(
    'file' => 'operations.node.inc',
  );
  $operations['TfDrupalNodeDelete'] = array(
    'file' => 'operations.node.inc',
  );
  $operations['TfDrupalNodeFieldsExtract'] = array(
    'file' => 'operations.node.inc',
  );
  $operations['TfDrupalNodeFieldsSet'] = array(
    'file' => 'operations.node.inc',
  );
  $operations['TfDrupalUserLoad'] = array(
    'file' => 'operations.user.inc',
  );
  $operations['TfDrupalUserModify'] = array(
    'file' => 'operations.user.inc',
  );
  $operations['TfDrupalUserCreate'] = array(
    'file' => 'operations.user.inc',
  );
  $operations['TfDrupalUserFieldsExtract'] = array(
    'file' => 'operations.user.inc',
  );
  return $operations;
}
