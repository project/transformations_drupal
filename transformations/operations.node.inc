<?php
/**
 * @file
 * Drupal Transformations -
 * Transformations for processing Drupal-native data like nodes, users, etc.
 *
 * Copyright 2009 by Jakob Petsovits ("jpetso", http://drupal.org/user/56020)
 */


/**
 * Helper class for providing implementations for standard input/output info.
 */
class TfNodeConnectionInfoHelper {
  public static function nodeObjectInfo($propertyKey, $nodeType = NULL) {
    switch ($propertyKey) {
      case 'label':
        return t('Node object');

      case 'expectedType':
        return isset($nodeType)
          ? 'drupal:node:object:' . $nodeType
          : 'drupal:node:object';
    }
  }

  public static function nodeTypeInfo($propertyKey) {
    switch ($propertyKey) {
      case 'label':
        return t('Content type');

      case 'expectedType':
        return 'drupal:node:type'; // is a 'php:type:string'
    }
  }

  public static function nodeFieldInfo($nodeField, $propertyKey) {
    switch ($propertyKey) {
      case 'label':
        return $nodeField['label'];

      default:
        $function = fieldtool_get_function(
          $nodeField, 'transformations slot property callback'
        );
        return $function($nodeField, $propertyKey);
    }
  }
}


/**
 * Implementation of [module]_operation_[class]().
 */
function transformations_drupal_operation_TfDrupalNodeLoad() {
  return array(
    'category' => t('Drupal nodes'),
    'label' => t('Load node'),
    'description' => t('Retrieves a Drupal node from the database.'),
  );
}

class TfDrupalNodeLoad extends TfOperation {
  /**
   * Implementation of TfOperation::inputs().
   */
  protected function inputs() {
    return array('nid', 'type');
  }

  /**
   * Implementation of TfOperation::inputInfo().
   */
  protected function inputInfo($inputKey, $propertyKey) {
    if ($inputKey == 'nid') {
      switch ($propertyKey) {
        case 'label':
          return t('Node id');

        case 'expectedType':
          return 'php:number';
      }
    }
    elseif ($inputKey == 'type') {
      switch ($propertyKey) {
        case 'required':
          return FALSE;

        case 'description':
          return t('If set, only nodes of the specified type will be loaded. (If the given nid corresponds to a node of a different type, this load operation will fail.) In return, the output schema will be more accurate and contain fields that specifically apply to this node.');

        default:
          return TfNodeConnectionInfoHelper::nodeTypeInfo($propertyKey);
      }
    }
  }

  /**
   * Overriding TfOperation::inputChanged().
   */
  protected function inputChanged($inputKey, $previousValue, $clearOutputCache = TfOperation::ClearOutputCache) {
    if ($inputKey == 'type') {
      if ($this->isInputSet($inputKey) && isset($previousValue)
          && $this->input($inputKey)->data() == $previousValue->data()) {
        return; // no changes
      }
      $this->updateOutputSchema('node');
    }
    parent::inputChanged($inputKey, $previousValue, TfOperation::ClearOutputCache);
  }

  /**
   * Implementation of TfOperation::outputs().
   */
  protected function outputs() {
    return array('node');
  }

  /**
   * Implementation of TfOperation::outputInfo().
   */
  protected function outputInfo($outputKey, $propertyKey) {
    if ($outputKey == 'node') {
      return TfNodeConnectionInfoHelper::nodeObjectInfo($propertyKey,
        $this->isInputSet('type') ? $this->input('type')->data() : NULL
      );
    }
  }

  /**
   * Implementation of TfOperation::execute().
   */
  protected function execute(TfOutput $output) {
    $param = array('nid' => $this->input('nid')->data());
    if ($this->isInputSet('type')) {
      $param['type'] = $this->input('type')->data();
    }
    $node = node_load($param);

    if (!$node) {
      $output->setErrorMessage(t('Could not load node with nid !nid.', array(
        '!nid' => $nid,
      )));
      return;
    }
    $output->set('node', $node);
  }
}


/**
 * Implementation of [module]_operation_[class]().
 */
function transformations_drupal_operation_TfDrupalNodeSave() {
  return array(
    'category' => t('Drupal nodes'),
    'label' => t('Save node'),
    'description' => t('Writes a Drupal node to the database.'),
  );
}

class TfDrupalNodeSave extends TfOperation {
  /**
   * Overriding TfOperation::hasSideEffects() to return TRUE.
   */
  public function hasSideEffects() {
    return TRUE;
  }

  /**
   * Implementation of TfOperation::inputs().
   */
  protected function inputs() {
    return array('node');
  }

  /**
   * Implementation of TfOperation::inputInfo().
   */
  protected function inputInfo($inputKey, $propertyKey) {
    if ($inputKey == 'node') {
      return TfNodeConnectionInfoHelper::nodeObjectInfo($propertyKey);
    }
  }

  /**
   * Implementation of TfOperation::outputs().
   */
  protected function outputs() {
    return array('savedNode');
  }

  /**
   * Implementation of TfOperation::outputInfo().
   */
  protected function outputInfo($outputKey, $propertyKey) {
    if ($outputKey == 'savedNode') {
      switch ($propertyKey) {
        case 'label':
          return t('Saved node object');

        case 'expectedType':
          return $this->inputProperty('node', 'assignedType');
      }
    }
  }

  /**
   * Implementation of TfOperation::execute().
   */
  protected function execute(TfOutput $output) {
    $node = $this->input('node')->data();
    $node = clone $node;
    $node = node_submit($node);
    node_save($node);
    $output->set('savedNode', $node);
  }
}


/**
 * Implementation of [module]_operation_[class]().
 */
function transformations_drupal_operation_TfDrupalNodeNew() {
  return array(
    'category' => t('Drupal nodes'),
    'label' => t('Create new node object'),
    'description' => t('Create a barebone Drupal node object. Results are not yet saved to the database.'),
  );
}

class TfDrupalNodeNew extends TfOperation {
  /**
   * Implementation of TfOperation::inputs().
   */
  protected function inputs() {
    return array('type', 'user');
  }

  /**
   * Implementation of TfOperation::inputInfo().
   */
  protected function inputInfo($inputKey, $propertyKey) {
    if ($inputKey == 'type') {
      return TfNodeConnectionInfoHelper::nodeTypeInfo($propertyKey);
    }
    elseif ($inputKey == 'user') {
      switch ($propertyKey) {
        case 'label':
          return t('Node author');

        case 'required':
          return FALSE;

        case 'expectedType';
          return 'drupal:user:object';
      }
    }
  }

  /**
   * Overriding TfOperation::inputChanged().
   */
  protected function inputChanged($inputKey, $previousValue, $clearOutputCache = TfOperation::ClearOutputCache) {
    if ($inputKey == 'type') {
      if ($this->isInputSet($inputKey) && isset($previousValue)
          && $this->input($inputKey)->data() == $previousValue->data()) {
        return; // no changes
      }
      $this->updateOutputSchema('newNode');
    }
    parent::inputChanged($inputKey, $previousValue, TfOperation::ClearOutputCache);
  }

  /**
   * Implementation of TfOperation::outputs().
   */
  protected function outputs() {
    $outputs = array();
    if ($this->isInputSet('type')) {
      $outputs[] = 'newNode';
    }
    return $outputs;
  }

  /**
   * Implementation of TfOperation::outputInfo().
   */
  protected function outputInfo($outputKey, $propertyKey) {
    if ($outputKey == 'newNode') {
      switch ($propertyKey) {
        case 'label':
          return t('New node object');

        default:
          return TfNodeConnectionInfoHelper::nodeObjectInfo(
            $propertyKey, $this->input('type')->data()
          );
      }
    }
  }

  /**
   * Implementation of TfOperation::execute().
   */
  protected function execute(TfOutput $output) {
    $node = new stdClass();
    $node->type = $this->input('type')->data();

    if ($this->isInputSet('user')) {
      $account = $this->input('user');
      $node->uid = $account->uid;
      $node->name = $account->name;
    }
    else {
      $node->uid = 0;
      $node->name = variable_get('anonymous', t('Anonymous'));
    }
    module_load_include('inc', 'node', 'node.pages');
    node_object_prepare($node);
    $output->set('newNode', $node);
  }
}


/**
 * Implementation of [module]_operation_[class]().
 */
function transformations_drupal_operation_TfDrupalNodeDelete() {
  return array(
    'category' => t('Drupal nodes'),
    'label' => t('Delete node'),
    'description' => t('Deletes a Drupal node from the database. Note that it cannot be recovered after it\'s gone!'),
  );
}

class TfDrupalNodeDelete extends TfOperation {
  /**
   * Implementation of TfOperation::inputs().
   */
  protected function inputs() {
    return array('nid');
  }

  /**
   * Implementation of TfOperation::inputInfo().
   */
  protected function inputInfo($inputKey, $propertyKey) {
    if ($inputKey == 'nid') {
      switch ($propertyKey) {
        case 'label':
          return t('Node id');

        case 'expectedType':
          return 'php:number';
      }
    }
  }

  /**
   * Implementation of TfOperation::outputs().
   */
  protected function outputs() {
    return array();
  }

  /**
   * Implementation of TfOperation::outputInfo().
   */
  protected function outputInfo($outputKey, $propertyKey) {
    return NULL;
  }

  /**
   * Implementation of TfOperation::execute().
   */
  protected function execute(TfOutput $output) {
    node_delete($this->input('nid')->data());
  }
}


/**
 * Implementation of [module]_operation_[class]().
 */
function transformations_drupal_operation_TfDrupalNodeFieldsExtract() {
  return array(
    'category' => t('Drupal nodes'),
    'label' => t('Extract fields from node object'),
    'description' => t('Retrieve data from an existing Drupal node object.'),
  );
}

class TfDrupalNodeFieldsExtract extends TfOperation {
  /**
   * Implementation of TfOperation::inputs().
   */
  protected function inputs() {
    return array('node', 'type');
  }

  /**
   * Implementation of TfOperation::inputInfo().
   */
  protected function inputInfo($inputKey, $propertyKey) {
    if ($inputKey == 'type') {
      switch ($propertyKey) {
        case 'required':
          return FALSE;

        default:
          return TfNodeConnectionInfoHelper::nodeTypeInfo($propertyKey);
      }
    }
    elseif ($inputKey == 'node') {
      return TfNodeConnectionInfoHelper::nodeObjectInfo($propertyKey);
    }
  }

  /**
   * Overriding TfOperation::inputChanged().
   */
  protected function inputChanged($inputKey, $previousValue, $clearOutputCache = TfOperation::ClearOutputCache) {
    if ($inputKey == 'type') {
      if ($this->isInputSet($inputKey) && isset($previousValue)
          && $this->input($inputKey)->data() == $previousValue->data()) {
        return; // no changes
      }
      $this->updateOutputSchema();
    }
    parent::inputChanged($inputKey, $previousValue, TfOperation::ClearOutputCache);
  }

  /**
   * Overriding TfOperation::inputTypeChanged().
   */
  protected function inputTypeChanged($inputKey, $previousType) {
    if ($inputKey == 'node') {
      $assignedType = $this->inputProperty('node', 'assignedType');

      /// TODO: This condition is in fact a bit of a hack, because the type
      /// could also be a subtype of "drupal:node:object:$typename". Ideally,
      /// this would be implemented using a parameterized type like
      /// "drupal:node:object<$typename>" and the typename extracted with
      /// TfDataType::extractTypeParameter().
      if (strpos($assignedType, 'drupal:node:object:') === FALSE) {
        $this->setInput('type', NULL);
      }
      else {
        $nodeType = substr($assignedType, strlen('drupal:node:object:'));
        $this->setInput('type', $nodeType);
      }
    }
  }

  /**
   * Implementation of TfOperation::outputs().
   */
  protected function outputs() {
    return array_keys(fieldtool_fields('node', array(
      'types' => $this->isInputSet('type')
        ? array($this->input('type')->data())
        : array(),
      'required keys' => array('transformations slot property callback', 'getter callback'),
    )));
  }

  /**
   * Implementation of TfOperation::outputInfo().
   */
  protected function outputInfo($outputKey, $propertyKey) {
    $nodeFields = fieldtool_fields('node', array(
      'required keys' => array('transformations slot property callback', 'getter callback'),
    ));
    return TfNodeConnectionInfoHelper::nodeFieldInfo($nodeFields[$outputKey], $propertyKey);
  }

  /**
   * Implementation of TfOperation::execute().
   */
  protected function execute(TfOutput $output) {
    $node = $this->input('node')->data();
    $nodeFieldInfo = fieldtool_fields('node', array(
      'types' => $this->isInputSet('type')
        ? array($this->input('type')->data())
        : array(),
      'required keys' => array('transformations slot property callback', 'getter callback'),
    ));
    foreach ($nodeFieldInfo as $fieldName => &$info) {
      $fieldData = fieldtool_get($info, $node);
      if (isset($fieldData)) {
        $output->set($fieldName, $fieldData);
      }
    }
  }
}


/**
 * Implementation of [module]_operation_[class]().
 */
function transformations_drupal_operation_TfDrupalNodeFieldsSet() {
  return array(
    'category' => t('Drupal nodes'),
    'label' => t('Set fields in node object'),
    'description' => t('Modify an existing Drupal node object. Results are not yet saved to the database.'),
  );
}

class TfDrupalNodeFieldsSet extends TfOperation {
  /**
   * Return the node/content type of the resulting modified node, or NULL if
   * the type is not known.
   */
  protected function nodeType() {
    if ($this->isInputSet('type')) {
      return $this->input('type')->data();
    }
    return NULL;
  }

  /**
   * Return an array with the result of the nodeType() method as single element,
   * or an empty array if nodeType() returns NULL.
   */
  protected function nodeTypeArray() {
    $type = $this->nodeType();
    return empty($type) ? array() : array($type);
  }

  /**
   * Implementation of TfOperation::inputs().
   */
  protected function inputs() {
    $inputs = array_keys(fieldtool_fields('node', array(
      'types' => $this->nodeTypeArray(),
      'required keys' => array('transformations slot property callback', 'setter callback'),
    )));
    array_unshift($inputs, 'node');
    return $inputs;
  }

  /**
   * Implementation of TfOperation::inputInfo().
   */
  protected function inputInfo($inputKey, $propertyKey) {
    if ($inputKey == 'node') {
      switch ($propertyKey) {
        case 'label':
          return t('Existing node object');

        default:
          return TfNodeConnectionInfoHelper::nodeObjectInfo($propertyKey);
      }
    }
    else { // handling field keys
      if ($propertyKey == 'required') {
        return FALSE;
      }
      $nodeFields = fieldtool_fields('node', array(
        'required keys' => array('transformations slot property callback', 'setter callback'),
      ));
      return TfNodeConnectionInfoHelper::nodeFieldInfo($nodeFields[$inputKey], $propertyKey);
    }
  }

  /**
   * Overriding TfOperation::inputChanged().
   */
  protected function inputChanged($inputKey, $previousValue, $clearOutputCache = TfOperation::ClearOutputCache) {
    if ($inputKey == 'type') {
      if ($this->isInputSet($inputKey) && isset($previousValue)
          && $this->input($inputKey)->data() == $previousValue->data()) {
        return; // no changes
      }

      $updatedKeys = array();
      foreach ($this->inputKeys() as $key) {
        if (!in_array($key, array('node', 'type'))) {
          $updatedKeys[] = $key;
        }
      }
      $this->updateInputSchema($updatedKeys);
      $this->updateOutputSchema('modifiedNode');
    }
    parent::inputChanged($inputKey, $previousValue, TfOperation::ClearOutputCache);
  }

  /**
   * Overriding TfOperation::inputTypeChanged().
   */
  protected function inputTypeChanged($inputKey, $previousType) {
    if ($inputKey == 'node') {
      $assignedType = $this->inputProperty('node', 'assignedType');

      /// TODO: This condition is in fact a bit of a hack, because the type
      /// could also be a subtype of "drupal:node:object:$typename". Ideally,
      /// this would be implemented using a parameterized type like
      /// "drupal:node:object<$typename>" and the typename extracted with
      /// TfDataType::extractTypeParameter().
      if (strpos($assignedType, 'drupal:node:object:') === FALSE) {
        $this->setInput('type', NULL);
      }
      else {
        $nodeType = substr($assignedType, strlen('drupal:node:object:'));
        $this->setInput('type', $nodeType);
      }
    }
  }

  /**
   * Implementation of TfOperation::outputs().
   */
  protected function outputs() {
    return array('node');
  }

  /**
   * Implementation of TfOperation::outputInfo().
   */
  protected function outputInfo($outputKey, $propertyKey) {
    if ($outputKey == 'node') {
      switch ($propertyKey) {
        case 'label':
          return t('Modified node object');

        case 'expectedType':
          if ($this->isInputSet('type')) {
            return 'drupal:node:object:' . $this->input('type')->data();
          }
          return $this->inputProperty('node', 'assignedType');
      }
    }
  }

  /**
   * Implementation of TfOperation::execute().
   */
  protected function execute(TfOutput $output) {
    $oldNode = $this->input('node')->data();
    $newNode = clone $oldNode;
    $nodeFieldInfo = fieldtool_fields('node', array(
      'types' => $this->nodeTypeArray(),
      'required keys' => array('transformations slot property callback', 'setter callback'),
    ));
    if (isset($nodeFieldInfo['node'])) {
      // Not aware of any module that provides a field named 'node',
      // but let's make sure to avoid those in case there is such a thing.
      unset($nodeFieldInfo['node']);
    }

    foreach ($nodeFieldInfo as $fieldName => &$info) {
      if (!$this->isInputSet($fieldName)) {
        // The user decides which fields to set and which ones to leave as is.
        continue;
      }
      $type = $this->input($fieldName)->type();

      if (TfDataType::subtypeOf($type, 'php:type:array')) {
        // Array data is the format that we want for field inputs.
        $data = $this->input($fieldName)->data();
      }
      elseif (TfDataType::subtypeOf($type, 'transformations:list')) {
        // A list that is not in array format should be converted to an array,
        // so that Field Tool can properly cope with it.
        $data = array();
        foreach ($this->input($fieldName)->children(TfDataWrapper::ChildrenAlwaysConcrete) as $key => $item) {
          $data[] = $item;
        }
      }
      else {
        // Let's just pass the data as is.
        $data = $this->input($fieldName)->data();
      }

      fieldtool_set($info, $newNode, $data);
    }
    $output->set('node', $newNode);
  }
}
